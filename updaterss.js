const fs = require('node:fs');
function readFile() {
    const json = fs.readFileSync('public/Posts/posts.json', 'utf8', (err, data) => {
    });
    return json;
}
async function func() {
    var buildDate = new Date(Date.now());
    const posts = await readFile();
    const feed = JSON.parse(posts);
    var rssfeed = `<rss version="2.0">
<channel>
<title>${feed.title}</title>
<link>https://meerkats.shays.is-a.dev/Blog.html</link>
<description/>
<language>en</language>
<lastBuildDate>${buildDate.toUTCString()}</lastBuildDate>`;

    for (const post in feed.items) {
        const itemTemplate = `
<item>
<title>${feed.items[post]['title']}</title>
<link>${feed.items[post]['url']}</link>
<pubDate>${feed.items[post]['date_published']}</pubDate>
<guid isPermaLink="false">${feed.items[post]['external_url']}</guid>
<description>${feed.items[post]['content_text']}</description>
<category>${feed.items[post]['tags']}</category>
</item>`;
        rssfeed += itemTemplate;
    }
    rssfeed += `
</channel>
</rss>`
    fs.writeFile('feed.rss', rssfeed, function (err) {});
    console.log(rssfeed);
}
func();
